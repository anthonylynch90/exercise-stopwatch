import { Component } from '@angular/core';
import {NavController, PopoverController} from 'ionic-angular';
import {TimeSetComponent} from "../timeSet/timeSet";
import { Storage } from '@ionic/storage';

export class TimeLapse {
  minutes: number;
  seconds: number;
}

export class TimeSetUp {
  name: string;
  open: boolean;
  preparation: TimeLapse;
  workout: TimeLapse;
  rest: TimeLapse;
  cycles: number;
}

@Component({
  selector: 'time-list-component',
  styleUrls: ['/components/timeList/timeList.css'],
  templateUrl: 'timeList.html'
})
export class TimeListComponent {

  public timeSetups: [TimeSetUp];
  public defaultSetup: TimeSetUp = {
    name: "Tabata",
    open: true,
    preparation: {minutes: 0, seconds: 5},
    workout: {minutes: 0, seconds: 20},
    rest: {minutes: 0, seconds: 10},
    cycles: 8
  }

  constructor(public navCtrl: NavController,
              public popoverCtrl: PopoverController,
              public storage: Storage) {
    this.setList();
  }

  public presentPopover(timeLapse: TimeLapse): void {
    let popover = this.popoverCtrl.create(TimeSetComponent,
      {timeSet: timeLapse});
    popover.present();
    popover.onDidDismiss((newTimeLapse: TimeLapse) => {
      if(newTimeLapse) {
        this.saveSetups();
      }
    });
  }

  public addTimer() {
    this.defaultSetup.name = "Workout " + this.timeSetups.length;
    this.timeSetups.push(this.defaultSetup);
    this.closeAllAccordiansExceptIndex(this.timeSetups.length);
    this.saveSetups();
  }

  public toggleSection(i) {
    this.timeSetups[i].open = !this.timeSetups[i].open;
    this.closeAllAccordiansExceptIndex(i);
    this.saveSetups();
  }

  private closeAllAccordiansExceptIndex(indexToIgnore: number) {
    this.timeSetups.forEach((timeSetup, index) => {
      if(index !== indexToIgnore) {
        timeSetup.open = false;
      }
    })
  }

  private setList(): void {
    this.storage.get("timesSetups")
      .then((times: [TimeSetUp]) => {
        this.timeSetups = times;
        if(!this.timeSetups) {
          this.timeSetups = [this.defaultSetup];
          this.saveSetups();
        }
      })
      .catch((error: any) => {
      });
  }

  private saveSetups(): void{
    this.storage.set("timesSetups", this.timeSetups)
      .then((times: [TimeSetUp]) => {
      })
      .catch((error: any) => {
      });
  }

}
