import {Component, Input} from '@angular/core';

export class TimeLapse {
  minutes: number;
  seconds: number;
}

@Component({
  selector: 'time-lapse-component',
  templateUrl: 'timeLapse.html'
})
export class TimeLapseComponent {

  @Input('time-lapse') timeLapse: TimeLapse;

  constructor() {
    console.log(this.timeLapse);
  }

}
