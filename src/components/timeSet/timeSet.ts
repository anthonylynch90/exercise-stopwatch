import {Component} from '@angular/core';
import {ViewController} from 'ionic-angular';

export class TimeLapse {
  minutes: number;
  seconds: number;
}

@Component({
  templateUrl: 'timeSet.html'
})
export class TimeSetComponent {

  public timeSet: TimeLapse;
  public secondsSplit: Array<number>;
  public minutesSplit: Array<number>;

  constructor(public viewCtrl: ViewController) {
    this.setupTime();
    this.viewCtrl.data = "ll";
  }

  public addTime(type: string, index: number): void {
    if(this[type][index] < 9) {
      this[type][index] += 1;
    }
  }

  public minusTime(type: string, index: number): void {
    if(this[type][index] > 0) {
      this[type][index] -= 1;
    }
  }

  public saveTime(): void {
    this.viewCtrl.dismiss(this.createTimeLapse());
  }

  private createTimeLapse(): TimeLapse {
    this.timeSet.seconds = parseInt(this.secondsSplit[0].toString().concat(this.secondsSplit[1].toString()));
    this.timeSet.minutes = parseInt(this.minutesSplit[0].toString().concat(this.minutesSplit[1].toString()));
    return this.timeSet;
  }

  private setupTime(): void{
    this.timeSet = this.viewCtrl.data.timeSet;
    this.secondsSplit = (this.timeSet.seconds + "").split("").map(val => Number(val));
    this.minutesSplit = (this.timeSet.minutes + "").split("").map(val => Number(val));
    if(this.secondsSplit.length <= 1){
      this.secondsSplit.unshift(0);
    }
    if(this.minutesSplit.length <= 1){
      this.minutesSplit.unshift(0);
    }
  }

}
